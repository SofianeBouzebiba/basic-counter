﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace biblio
{
    public class compteurClass
    {
        public static int Valeur;
        public static int IncrémentationCompteur()
        {
            Valeur = Valeur + 1;
            return Valeur;
        }
        public static int DécrementationCompteur()
        {
            if (Valeur > 0)
            {
                Valeur = Valeur - 1;
            }
            return Valeur;
        }
        public static int RemiseAZero()
        {
            Valeur = 0;
            return Valeur;
        }
    }
}
