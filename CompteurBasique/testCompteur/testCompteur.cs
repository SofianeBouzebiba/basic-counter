﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using biblio;

namespace testCompteur
{
    [TestClass]
    public class testCompteur
    {

        [TestMethod]
        public void TestRAZ()
        {
            Assert.AreEqual(0, compteurClass.RemiseAZero());
        }
        [TestMethod]
        public void TestDécrementation()
        {
            Assert.AreEqual(0, compteurClass.RemiseAZero());
            Assert.AreEqual(1, compteurClass.IncrémentationCompteur());
            Assert.AreEqual(2, compteurClass.IncrémentationCompteur());
            Assert.AreEqual(1, compteurClass.DécrementationCompteur());
        }
        [TestMethod]
        public void TestIncrémentation()
        {
            Assert.AreEqual(0, compteurClass.RemiseAZero());
            Assert.AreEqual(1, compteurClass.IncrémentationCompteur());
            Assert.AreEqual(2, compteurClass.IncrémentationCompteur());
        }
    }
}
