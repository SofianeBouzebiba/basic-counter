﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicPage
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_décrementation = New System.Windows.Forms.Button()
        Me.lbl_Valeur = New System.Windows.Forms.Label()
        Me.title = New System.Windows.Forms.Label()
        Me.btn_incrémentation = New System.Windows.Forms.Button()
        Me.btn_RAZ = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btn_décrementation
        '
        Me.btn_décrementation.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_décrementation.AutoSize = True
        Me.btn_décrementation.BackColor = System.Drawing.Color.White
        Me.btn_décrementation.FlatAppearance.BorderSize = 0
        Me.btn_décrementation.Font = New System.Drawing.Font("Arial", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_décrementation.Location = New System.Drawing.Point(55, 92)
        Me.btn_décrementation.Name = "btn_décrementation"
        Me.btn_décrementation.Size = New System.Drawing.Size(100, 35)
        Me.btn_décrementation.TabIndex = 11
        Me.btn_décrementation.Text = "-"
        Me.btn_décrementation.UseVisualStyleBackColor = False
        '
        'lbl_Valeur
        '
        Me.lbl_Valeur.Font = New System.Drawing.Font("Arial", 45.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Valeur.Location = New System.Drawing.Point(161, 81)
        Me.lbl_Valeur.Name = "lbl_Valeur"
        Me.lbl_Valeur.Size = New System.Drawing.Size(163, 63)
        Me.lbl_Valeur.TabIndex = 10
        Me.lbl_Valeur.Text = "0"
        Me.lbl_Valeur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'title
        '
        Me.title.AutoSize = True
        Me.title.Font = New System.Drawing.Font("Arial", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.title.Location = New System.Drawing.Point(210, 27)
        Me.title.Name = "title"
        Me.title.Size = New System.Drawing.Size(52, 23)
        Me.title.TabIndex = 9
        Me.title.Text = "Total"
        '
        'btn_incrémentation
        '
        Me.btn_incrémentation.AutoSize = True
        Me.btn_incrémentation.BackColor = System.Drawing.Color.White
        Me.btn_incrémentation.FlatAppearance.BorderSize = 0
        Me.btn_incrémentation.Font = New System.Drawing.Font("Arial", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_incrémentation.Location = New System.Drawing.Point(330, 92)
        Me.btn_incrémentation.Name = "btn_incrémentation"
        Me.btn_incrémentation.Size = New System.Drawing.Size(100, 35)
        Me.btn_incrémentation.TabIndex = 8
        Me.btn_incrémentation.Text = "+"
        Me.btn_incrémentation.UseVisualStyleBackColor = False
        '
        'btn_RAZ
        '
        Me.btn_RAZ.AutoSize = True
        Me.btn_RAZ.BackColor = System.Drawing.Color.White
        Me.btn_RAZ.FlatAppearance.BorderSize = 0
        Me.btn_RAZ.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.btn_RAZ.Location = New System.Drawing.Point(205, 192)
        Me.btn_RAZ.Margin = New System.Windows.Forms.Padding(0)
        Me.btn_RAZ.Name = "btn_RAZ"
        Me.btn_RAZ.Size = New System.Drawing.Size(77, 43)
        Me.btn_RAZ.TabIndex = 7
        Me.btn_RAZ.Text = "RàZ"
        Me.btn_RAZ.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(484, 262)
        Me.Controls.Add(Me.btn_décrementation)
        Me.Controls.Add(Me.lbl_Valeur)
        Me.Controls.Add(Me.title)
        Me.Controls.Add(Me.btn_incrémentation)
        Me.Controls.Add(Me.btn_RAZ)
        Me.MaximumSize = New System.Drawing.Size(500, 301)
        Me.MinimumSize = New System.Drawing.Size(500, 301)
        Me.Name = "Form1"
        Me.Text = " "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_décrementation As Button
    Friend WithEvents lbl_Valeur As Label
    Friend WithEvents title As Label
    Friend WithEvents btn_incrémentation As Button
    Friend WithEvents btn_RAZ As Button
End Class
