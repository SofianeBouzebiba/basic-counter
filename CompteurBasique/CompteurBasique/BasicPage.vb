﻿
Imports biblio

Public Class BasicPage
    Private Sub btn_incrémentation_Click(sender As Object, e As EventArgs) Handles btn_incrémentation.Click
        lbl_Valeur.Text = compteurClass.IncrémentationCompteur()
    End Sub

    Private Sub btn_RAZ_Click(sender As Object, e As EventArgs) Handles btn_RAZ.Click
        lbl_Valeur.Text = compteurClass.RemiseAZero()
    End Sub

    Private Sub btn_décrementation_Click_1(sender As Object, e As EventArgs) Handles btn_décrementation.Click
        lbl_Valeur.Text = compteurClass.DécrementationCompteur()
    End Sub
End Class

